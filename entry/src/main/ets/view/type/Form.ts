export interface CheckItem {
  id?: number
  index?: number
  value?: string
  checked?: boolean
}

export type FormValueType = string | Date | number | string[] | CheckItem[] | undefined

export interface RuleItem {
  [key: string]: string | number | boolean | RegExp | Function
}

export interface Rules {
  [key: string]: RuleItem[]
}

export interface FormVals {
  [key: string]: FormValueType
}