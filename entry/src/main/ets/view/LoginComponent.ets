import router from '@ohos.router';
import { CommonConstants } from '../common/constants/CommonConstants';
import { getCodeImg, login } from '../api/login';
import { setToken, setUserState } from '../common/store';
import { UserState } from '../common/store/UserState'
import { globalCfg } from '../config/setting'
import { login as loginCfg, global } from '../config/theme'
import { Log } from '../common/utils/log'
import { Captcha, UserInfo } from '../api/model'

/**
 * 登录组件
 */
@Component
export struct LoginComponent {
  @StorageProp('isMinHeight') isMinHeight: boolean = false;
  @State userName: string = '';
  @State password: string = '';
  @State captchaEnabled: boolean = true;
  @State codeUrl: string = '';
  @State uuid?: string = '';
  @State captchaCode: string = '';
  @State loading: boolean = false;

  aboutToAppear() {
    Log.info('登录页面开始')
    this.captchaEnabled = globalCfg.captchaEnabled
    this.getCaptchaCode();
  }

  getCaptchaCode() {
    if (this.captchaEnabled) {
      Log.info('调用getCaptchaCode获取验证码')
      getCodeImg<Captcha>().then((res: Captcha) => {
        this.codeUrl = 'data:image/gif;base64,' + res.img;
        this.uuid = res.uuid;
        Log.info('获取验证码成功', this.codeUrl)
      }).catch((e: Error) => {
        Log.error('获取验证码发生错误', e.message)
      });
    }
  }

  @Builder
  renderUserName() {
    TextInput({ placeholder: $r('app.string.username') })
      .width(CommonConstants.TEXT_INPUT_WIDTH_PERCENT)
      .height($r('app.float.text_input_height'))
      .placeholderColor($r('app.color.text_input_place_holder'))
      .placeholderFont({ size: $r('app.float.text_input_font_size') })
      .backgroundColor($r('app.color.login_input_text_background'))
      .fontSize($r('app.float.text_input_font_size'))
      .onChange((value: string) => {
        this.userName = value;
      })
  }

  @Builder
  renderPassword() {
    TextInput({ placeholder: $r('app.string.password') })
      .width(CommonConstants.TEXT_INPUT_WIDTH_PERCENT)
      .height($r('app.float.text_input_height'))
      .placeholderColor($r('app.color.text_input_place_holder'))
      .placeholderFont({ size: $r('app.float.text_input_font_size') })
      .fontSize($r('app.float.text_input_font_size'))
      .backgroundColor($r('app.color.login_input_text_background'))
      .type(InputType.Password)
      .onChange((value: string) => {
        this.password = value;
      })
      .margin({ top: 10 })
  }

  @Builder
  renderCaptcha() {
    GridRow() {
      GridCol({
        span: 6
      }) {
        TextInput({ placeholder: $r('app.string.captcha_code') })
          .width('100%')
          .height($r('app.float.text_input_height'))
          .placeholderColor($r('app.color.text_input_place_holder'))
          .placeholderFont({ size: $r('app.float.text_input_font_size') })
          .fontSize($r('app.float.text_input_font_size'))
          .backgroundColor($r('app.color.login_input_text_background'))
          .margin({ bottom: $r('app.float.input_margin_bottom') })
          .onChange((value: string) => {
            this.captchaCode = value;
          })
      }

      GridCol({
        span: 6
      }) {
        Image(this.codeUrl).height(50).onClick(() => {
          this.getCaptchaCode()
        })
      }
    }.margin({ top: 10 })
  }

  @Builder
  renderLoginBt() {
    Button() {
      Row() {
        if (this.loading) {
          Image($r('app.media.loading'))
            .width(40)
            .height(40)
            .margin({ right: 5 })
          Text('登录中...').fontSize($r('app.float.text_input_font_size'))
            .fontWeight(CommonConstants.LOGIN_TEXT_FONT_WEIGHT)
            .fontColor(this.isLoginButtonClickable() ? Color.White : $r('app.color.login_font_normal'))
        } else {
          Text($r('app.string.login'))
            .fontSize($r('app.float.text_input_font_size'))
            .fontWeight(CommonConstants.LOGIN_TEXT_FONT_WEIGHT)
            .fontColor(this.isLoginButtonClickable() ? Color.White : $r('app.color.login_font_normal'))
        }
      }.alignItems(VerticalAlign.Center)
    }
    .width(CommonConstants.BUTTON_WIDTH_PERCENT)
    .height($r('app.float.login_btn_height'))
    .borderRadius($r('app.float.login_btn_border_radius'))
    .margin({ top: $r('app.float.register_btn_margin_top') })
    .enabled(this.isLoginButtonClickable())
    .backgroundColor(this.isLoginButtonClickable() ? $r('app.color.login_btn_enabled') :
    $r('app.color.login_btn_normal'))
    .onClick(() => {
      this.loading = true
      login<UserInfo>(this.userName, this.password, this.captchaCode, this.uuid).then((res: UserInfo) => {
        //缓存token
        setToken(res.token);
        //获取用户信息
        let userState = new UserState();
        userState.loadUserInfo().then(() => {
          Log.info('加载登录用户信息成功')
          this.loading = false;
          router.replaceUrl({
            url: CommonConstants.HOME_PAGE_URL
          });
        }).catch(() => {
          this.loading = false;
        });
      }).catch(() => {
        this.loading = false;
      })
    })
  }

  @Builder
  renderRegisterBt() {
    Button($r('app.string.register'))
      .width(CommonConstants.BUTTON_WIDTH_PERCENT)
      .height($r('app.float.login_btn_height'))
      .fontSize($r('app.float.text_input_font_size'))
      .fontWeight(CommonConstants.BUTTON_FONT_WEIGHT)
      .borderRadius($r('app.float.register_btn_border_radius'))
      .margin({ top: $r('app.float.register_btn_margin_top') })
      .fontColor($r('app.color.register_font_clickable'))
      .backgroundColor($r('app.color.register_btn_clickable'))
      .onClick(() => {
        router.pushUrl({
          url: CommonConstants.REGISTRATION_SUCCESS_PAGE_URL
        });
      })
  }

  build() {
    Column() {
      Row() {
        Image(global.logo).width(50).height(50);
        Text(loginCfg.subtitle.text).fontSize(loginCfg.subtitle.size).margin({ left: 10 })
      }
      .justifyContent(FlexAlign.Center)
      .height('150vp')

      //表单
      GridRow({
        columns: {
          sm: CommonConstants.GRID_ROW_SM,
          md: CommonConstants.GRID_ROW_MD,
          lg: CommonConstants.GRID_ROW_LG
        },
        gutter: { x: CommonConstants.GUTTER_X },
        breakpoints: { value: CommonConstants.BREAK_POINT }
      }) {
        GridCol({
          span: {
            sm: CommonConstants.SPAN_SM,
            md: CommonConstants.SPAN_MD,
            lg: CommonConstants.SPAN_LG
          },
          offset: {
            sm: CommonConstants.OFFSET_SM,
            md: CommonConstants.OFFSET_MD,
            lg: CommonConstants.OFFSET_LG
          }
        }) {
          Column() {
            //用户名输入
            this.renderUserName()
            //密码输入
            this.renderPassword()
            //验证码输入
            this.renderCaptcha()
          }
        }

        //登录按钮
        GridCol({
          span: {
            sm: CommonConstants.BUTTON_SPAN_SM,
            md: this.isMinHeight ? CommonConstants.BUTTON_SPAN_MD_SMALL : CommonConstants.BUTTON_SPAN_MD_BIG,
            lg: CommonConstants.BUTTON_SPAN_LG
          },
          offset: {
            sm: CommonConstants.BUTTON_OFFSET_SM,
            md: CommonConstants.BUTTON_OFFSET_MD,
            lg: CommonConstants.BUTTON_OFFSET_LG
          }
        }) {
          this.renderLoginBt()
        }

        //注册按钮
        GridCol({
          span: {
            sm: CommonConstants.BUTTON_SPAN_SM,
            md: this.isMinHeight ? CommonConstants.BUTTON_SPAN_MD_SMALL : CommonConstants.BUTTON_SPAN_MD_BIG,
            lg: CommonConstants.BUTTON_SPAN_LG
          },
          offset: {
            sm: CommonConstants.BUTTON_OFFSET_SM,
            md: this.isMinHeight ? CommonConstants.BUTTON_OFFSET_SM : CommonConstants.BUTTON_OFFSET_MD,
            lg: CommonConstants.BUTTON_OFFSET_LG
          }
        }) {
          //this.renderRegisterBt()
        }
      }
    }
  }

  isLoginButtonClickable(): boolean {
    if (this.loading) {
      return false;
    }
    return this.userName !== '' && this.password !== '' && this.captchaCode != '';
  }
}

