import { popMsg } from '../common/utils/ruoyi';
import router from '@ohos.router';
import { CommonConstants } from '../common/constants/CommonConstants';
import { ExtRouterOpts } from './type/Common'
import { Log } from '../common/utils/log';

@Observed
export class ListNav {
  menuList: Array<ListNavItem> = []

  constructor() {
  }

  add(nav: ListNavItem) {
    this.menuList.push(nav)
    return this
  }
}

@Observed
export class ListNavItem {
  ico: string | PixelMap | Resource //菜单图标
  text: string | Resource //菜单文字
  routerOpts?: ExtRouterOpts
  fontColor?: ResourceColor //字体颜色
  icoColor?: ResourceColor //图标颜色

  constructor(ico: string | PixelMap | Resource, text: string | Resource, routerOpts?: ExtRouterOpts) {
    this.ico = ico
    this.text = text
    this.routerOpts = routerOpts
  }

  setFontColor(fontColor: ResourceColor) {
    this.fontColor = fontColor
    return this
  }

  setIcoColor(icoColor: ResourceColor) {
    this.icoColor = icoColor
    return this
  }
}

/**
 * 列表导航组件
 */
@Component
export struct ListNavComp {
  @ObjectLink listNav: ListNav

  aboutToAppear(): void {
    Log.info('显示列表导航组件', this.listNav)
  }

  build() {
    //if (this.listNav && this.listNav.menuList) {
    List() {
      ForEach(this.listNav.menuList, (item: ListNavItem) => {
        ListItem() {
          Row() {
            Row() {
              if (item && item.ico) {
                Image(item.ico)
                  .height(20)
                  .width(20)
                  .objectFit(ImageFit.Contain)
                  .fillColor(item.icoColor ? item.icoColor : $r('sys.color.ohos_id_color_text_hyperlink_contrary'))
              }
              Text(item.text)
                .height($r('app.float.item_text_height'))
                .fontSize($r('sys.float.ohos_id_text_size_body2'))
                .fontWeight(CommonConstants.TEXT_FONT_WEIGHT)
                .fontColor(item.fontColor ? item.fontColor : $r('sys.color.ohos_id_color_foreground'))
                .margin({ left: 5 })
            }
            .width('95%')
            .justifyContent(FlexAlign.Start)
            .alignItems(VerticalAlign.Center)

            Image($r('app.media.ic_public_arrow_right'))
              .width(20)
              .height(20)
              .objectFit(ImageFit.Contain)
              .fillColor(item.icoColor ? item.icoColor : $r('sys.color.ohos_id_color_text_hyperlink_contrary'))
          }
          .height(50)
          .justifyContent(FlexAlign.SpaceBetween)
          .alignItems(VerticalAlign.Center)
        }
        .onClick(() => {
          if (item && item.routerOpts && item.routerOpts.url) {
            router.pushUrl(item.routerOpts)
          } else {
            popMsg('暂未实现')
          }
        })
      })
    }
    .divider({
      strokeWidth: '0.5vp',
      color: $r('app.color.item_divider')
    })

    //}
  }
}