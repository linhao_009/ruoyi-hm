import { popMsg } from '../common/utils/ruoyi';
import router from '@ohos.router';

@Observed
export class TitleBar {
  ico: string | PixelMap | Resource; //标题图标
  icoColor: ResourceColor; //图标颜色
  title: string | Resource; //标题
  fontColor: ResourceColor; //标题颜色
  more: boolean; //是否显示更多
  routerOpts : router.RouterOptions;

  constructor(
    ico: string | PixelMap | Resource,
    icoColor: ResourceColor,
    title: string | Resource,
    fontColor: ResourceColor,
    routerOpts : router.RouterOptions,
    more?: boolean
  ) {
    this.ico = ico
    this.icoColor = icoColor ? icoColor : $r('sys.color.ohos_id_color_titlebar_bg_dark')
    this.title = title
    this.fontColor = fontColor ? icoColor : $r('sys.color.ohos_id_color_titlebar_bg_dark')
    this.routerOpts = routerOpts
    this.more = more ? more : false;
  }
}
/**
 * 标题栏组件
 */
@Component
export struct TitleBarComp {
  @ObjectLink titleBar: TitleBar;

  build() {
    Row() {
      Row() {
        Image(this.titleBar.ico)
          .fillColor(this.titleBar.icoColor)
          .height(16)
        Text(this.titleBar.title)
          .fontWeight(FontWeight.Bolder)
          .fontSize(16)
          .margin({ left: 5 })
          .fontColor(this.titleBar.fontColor)
      }

      if (this.titleBar.more) {
        Row() {
          Text('更多')
            .fontSize(14)
            .margin({ right: 5 })
            .fontColor(this.titleBar.fontColor)
          Image($r('app.media.ic_public_arrow_right'))
            .fillColor(this.titleBar.fontColor)
            .height(14)
        }
        .margin({ right: 15 })
        .onClick(() => {
          if (this.titleBar && this.titleBar.routerOpts.url) {
            router.pushUrl({
              url: this.titleBar.routerOpts.url,
              params: this.titleBar.routerOpts.params,
            })
          } else {
            popMsg('暂未实现')
          }
        })
      }
    }
    .width('100%')
    .margin({ top: 5, left: 10, right: 10, bottom: 5 })
    .justifyContent(FlexAlign.SpaceBetween)
  }
}