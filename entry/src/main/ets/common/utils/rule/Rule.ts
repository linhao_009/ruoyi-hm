import prompt from '@ohos.promptAction';

export interface Rule {
  validate(val, rule, form): Promise<boolean>;
}
//弹出错误提示
export function popMsg(msg) {
  let error = msg ? msg : '必填项不能为空'
  prompt.showDialog({
    title: '错误',
    message: error,
    buttons: [
      {
        text: '确定',
        color: '#000000',
      }
    ],
  }).then((data) => {
    // 点击按钮处理，暂时忽略
  })
}