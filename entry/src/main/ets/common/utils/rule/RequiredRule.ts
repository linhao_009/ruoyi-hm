import { Log } from '../log';
import { Rule, popMsg } from './Rule'

export class RequiredRule implements Rule {
  async validate(val: any, rule: any, form: any): Promise<boolean> {
    if (rule['required']) {
      if (val instanceof Array) {
        if (val.length === 0) {
          popMsg(rule['errorMessage'])
          return false;
        }
      } else {
        if (!val) {
          popMsg(rule['errorMessage'])
          return false;
        }
      }
    }
    return true;
  }
}