import { globalCfg } from '../../config/setting'

export const Log = {
  info(message: string, ...args: any[]): void {
    console.info(`[${globalCfg.logTag}] ${message}`, args)
  },

  error(message: string, ...args: any[]): void {
    console.error(`[${globalCfg.logTag}] ${message}`, args)
  },

  warn(message: string, ...args: any[]): void {
    console.error(`[${globalCfg.logTag}] ${message}`, args)
  }
}