import NavTitleComponent from '../../view/NavTitleComponent'
import { global } from '../../config/theme'
import { CommonConstants } from '../../common/constants/CommonConstants';
import prompt from '@ohos.promptAction';
import { Log } from '../../common/utils/log';
import { getUserProfile, updateUserProfile } from '../../api/user'
import { popMsg } from '../../common/utils/ruoyi';
import { FormComp, FormField, Dic, FieldType, Form } from '../../view/FormComponent'
import { DicItem } from '../../view/type/Common'
import { Rules } from '../../view/type/Form'
import { ProfileApiResult, UserProfile } from '../../api/model';

//定义字典
const sexDic: DicItem[] = [
  { label: '', value: '男' },
  { label: '', value: '女' },
]

//字典数据格式化函数
function formatSexOpts(index: number) {
  switch (index) {
    case 0:
      return '男'
    case 1:
      return '女'
    default:
      return '请选择'
  }
}

/**
 * 编辑个人信息页面
 */
@Entry
@Component
struct EditProfilePage {
  @State loading: boolean = false;
  @State title: string = '编辑资料';
  @State form: Form = new Form();
  @State rules: Rules = {
    nickName: [
      {
        required: true,
        errorMessage: '用户昵称不能为空'
      }
    ],
    phonenumber: [
      {
        required: true,
        errorMessage: '手机号不能为空'
      },
      {
        pattern: /^1[3|4|5|6|7|8|9][0-9]\d{8}$/,
        errorMessage: '请输入正确的手机号码'
      }
    ],
    email: [
      {
        required: true,
        errorMessage: '邮箱地址不能为空'
      },
      {
        format: 'email',
        errorMessage: '请输入正确的邮箱地址'
      }
    ],
  }

  onPageShow() {
    getUserProfile<ProfileApiResult>().then((res: ProfileApiResult) => {
      const user: UserProfile = res.data
      this.initForm(user)
    })
  }

  //初始化form
  initForm(user: UserProfile) {
    this.form = new Form()
      .setRules(this.rules)
      .add(new FormField('用户昵称', 'nickName', user.nickName, true, FieldType.input))
      .add(new FormField('手机号', 'phonenumber', user.phonenumber, true, FieldType.input))
      .add(new FormField('邮箱', 'email', user.email, true, FieldType.input))
      .add(new FormField('性别', 'sex', user.sex ? parseInt(user.sex) : '', false, FieldType.select,
        new Dic(sexDic, formatSexOpts)))
  }

  //显示标题
  @Builder
  renderTitle() {
    Column() {
      NavTitleComponent({
        title: this.title,
        back: true,
        backUrl: '',
      })
    }
    .width('100%')
    .height('100%')
  }

  //显示页面主体
  @Builder
  renderBody() {
    Column() {
      this.renderForm();
      this.renderBt();
    }
    .height('100%')
    .width('100%')
    .backgroundColor(global.bodyBackgroundColor)
  }

  //显示表单
  @Builder
  renderForm() {
    FormComp({ form: this.form })
  }

  //显示保存按钮
  @Builder
  renderBt() {
    Button() {
      Row() {
        if (this.loading) {
          Image($r('app.media.loading'))
            .width(40)
            .height(40)
            .margin({ right: 5 })
          Text('保存中...').fontSize($r('app.float.text_input_font_size'))
            .fontWeight(CommonConstants.LOGIN_TEXT_FONT_WEIGHT)
            .fontColor(this.isButtonClickable() ? Color.White : $r('app.color.login_font_normal'))
        } else {
          Text('保存')
            .fontSize($r('app.float.text_input_font_size'))
            .fontWeight(CommonConstants.LOGIN_TEXT_FONT_WEIGHT)
            .fontColor(this.isButtonClickable() ? Color.White : $r('app.color.login_font_normal'))
        }
      }.alignItems(VerticalAlign.Center)
    }
    .width('95%')
    .height($r('app.float.login_btn_height'))
    .borderRadius($r('app.float.login_btn_border_radius'))
    .margin({ top: $r('app.float.register_btn_margin_top') })
    .enabled(this.isButtonClickable())
    .backgroundColor(this.isButtonClickable() ? $r('app.color.login_btn_enabled') : $r('app.color.login_btn_normal'))
    .onClick(() => {
      this.form.validate().then((valid: boolean) => {
        if (valid) {
          this.loading = true
          Log.info('form校验通过')
          let formVal = this.form.getFormVals()
          updateUserProfile(formVal).then((res: void) => {
            this.loading = false
            popMsg('保存成功')
          }).catch(() => {
            this.loading = false
          })
        }
      })
    })
  }

  isButtonClickable(): boolean {
    if (this.loading) {
      return false;
    }
    return true;
  }

  build() {
    Navigation() {
      this.renderBody()
    }
    .title(this.renderTitle())
  }
}