export interface ApiResult {
  data?: any
  code?: string
  msg?: string
}

export interface Captcha extends ApiResult {
  captchaEnabled?: boolean
  img?: string
  uuid?: string
}

export interface UserProfile extends ApiResult {
  userName?: string
  nickName?: string
  phonenumber?: string
  sex?: string
  age?: string
  email?: string
  avatar?: string
  status?: string,
  country?: string,
  birthday?: string,
  createTime?: string
  roleGroup?: string
  postGroup?: string
}

export interface UserInfo extends ApiResult {
  token?: string
  user?: UserProfile
  roles?: Array<object>
  permissions?: Array<string>
}

export interface ProfileApiResult extends ApiResult {
  roleGroup?: string
  postGroup?: string
}