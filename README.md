
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">RuoYi-HM v1.0.0</h1>
<h4 align="center">若依移动端框架（鸿蒙版）</h4>

## 平台简介

RuoYi-HM APP解决方案，采用华为鸿蒙框架使用ArkTs和TS语言开发，实现了与[RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue)对接的移动鸿蒙版解决方案！目前已经实现登录、我的、工作台、等基础功能。

## 版本更新

### 1.mater分支支持鸿蒙4.0版本
### 2.dev分支支持鸿蒙最新NEXT版本

## dev分支更新
### 1.支持API12
### 2.form表单组件更新
支持checkbox、radio、文本选择、日期选择、时间选择
### 3.form表单校验更新
支持自定义函数校验、远程校验

## 技术文档
<!-- TOC -->
  * 鸿蒙开发环境搭建 [参考文档](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/environment-0000001053662422-V3)
  * 鸿蒙开发快速入门 [参考文档](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/start-overview-0000001478061421-V3)
  * 工程编译构建 [参考文档](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/build_overview-0000001055075201-V3)
  * ArkUI开发入门 [参考文档](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/arkts-ui-development-overview-0000001438467628-V3)
  * 项目开发手册 [项目开发手册](https://luo-ji-xiang-liang-ke-ji.gitbook.io/ruo-yi-yi-dong-duan-hong-meng-ban/)
<!-- TOC -->


## 演示图

| ![](/screenshots/1.png) | ![](/screenshots/2.png) | ![](/screenshots/3.png) |
|-------------------------|-------------------------|-------------------------|
| ![](/screenshots/4.png) | ![](/screenshots/5.png) | ![](/screenshots/6.png) |


## 技术交流群
有关技术问题请加入群，一期探讨，互相学习！
### QQ群
![QQ群](/screenshots/qq-qr.png)
